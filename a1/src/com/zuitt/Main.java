package com.zuitt;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scannerInput = new Scanner(System.in);

        System.out.println("What is your first name?");
        String firstName = scannerInput.nextLine();

        System.out.println("What is your last name?");
        String lastName = scannerInput.nextLine();

        System.out.println("What is your first subject grade?");
        double firstSubject = scannerInput.nextDouble();

        System.out.println("What is your second subject grade?");
        double secondSubject = scannerInput.nextDouble();

        System.out.println("What is your third subject grade?");
        double thirdSubject = scannerInput.nextDouble();

        double average = (firstSubject + secondSubject + thirdSubject)/3;
        Double newValue = new Double(average);
        int result = newValue.intValue();

        System.out.println("Good Day, " + firstName + " " +  lastName + ".");
        System.out.println("Your grade average is: " + result);

        
    }
}
