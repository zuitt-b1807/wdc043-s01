package com.zuitt.example;

import java.util.Scanner;

public class Main {
/*    Main Class
          The Main class is the entry point of our Java program.
          It is responsible for executing our code. Every Java program
          should have at least 1 class and 1 function inside it.
          ctrl+/ for single comments - //
          ctrl+shift+/ - for multi-line comment
 */
    public static void main(String[] args) {
        /*
            -main function is where most executable code is applied to.
            -You can create a intelliJ Java project with a template that already uses
            or has a main class and function

            -The 'public' keyword is what we call an access modifier, it tells our application what parts of the program can access the main function.

            -The 'void' is the return statement's data type of the 'main' function that it defines what kind of data will be returned. 'void' means that the main function will return nothing.

             System.out.println() is a statement which will allow us to print the value of the argument passed into it
        */
        System.out.println("Hello World!");

        //variables and data types

        //In Java, to be able to declare a variable, we have to identify or declare its data type. Which means that the variable will expect and only accept data with the type declared.
        int myNum;
        myNum = 3;
        System.out.println(myNum);

//        myNum = "sample string";
        myNum = 50;
        System.out.println(myNum);

        //byte = -128 to 127
        byte students = 127;
        //short = -32,768 to 32,767
        short seats = 32767;
        System.out.println(students);
        System.out.println(seats);
        //int -2,147,483,648 to 2,147,483,647
        int localPopulation = 2147483647;
        //L is added at the end of the long number to be recognized as long. if that is omitted, it will wrongfully recognized as int.
        //long 786,881,145L
        long wordPopulation = 7862881145L;
        System.out.println(localPopulation);
        System.out.println(wordPopulation);
        //float can only have 7 digits (includes number before decimal point) AND the final decimal place is being round off
        float piFloat = 3.1415921f;
        //double - up to 16 decimal digits
        double piDouble = 3.1415926123456789;
        System.out.println(piFloat);
        System.out.println(piDouble);
        //char
        char letter ='a';
        //In Java, '' is a char literal to create char data type.
        letter ='b';
        //"" is for string literal.
//        letter ="b";
        System.out.println(letter);

        //boolean
        boolean isMVP = true;
        boolean isChampion = false;
        System.out.println(isMVP);
        System.out.println(isChampion);

        //final
        final int PRINCIPAL = 3000;
//        PRINCIPAL = 4000;
        System.out.println(PRINCIPAL);

        //String - In Java, is non-primitive data type. Because in Java, strings are objects that can use methods.
        //Non-primitive data types have access to methods.
        //Array,Class,Interface

        String username = "chesterSmith29";
        System.out.println(username);

        System.out.println(username.isEmpty());
        //.isEmpty() is a method if a string which returns a boolean.
        //it will check the length of the string and return true, if the length is 0, it will return false, if otherwise

        username = "";
        System.out.println(username.isEmpty());

        //Scanner - is a class which allows us to input into our program, it's kind of similar to prompt() in JS.
        //However, Scanner, being a class, has to be imported to be used.
        //Scanner outputs a string.
        //.nextLine() allows us to accept the whole next line of the user's input.
        Scanner scannerName = new Scanner(System.in);

        System.out.println("What is our name?");
        String myName = scannerName.nextLine();

        System.out.println("Your name is " + myName + "!");
        //nextInt() allows us to accept and store the next integer by our user's input.
        System.out.println("What is your age?");
        int myAge = scannerName.nextInt();
        System.out.println("You are " + myAge + " years old");

        System.out.println("What is your first quarter grade?");
        double firstQuarter = scannerName.nextDouble();

        System.out.println("Your first quarter grade is " + firstQuarter + "!");

        System.out.println("Enter a number:");
        int number1 = scannerName.nextInt();
        System.out.println("Enter a number:");
        int number2 = scannerName.nextInt();
        //Much like in Javascript we can operators like +,/ for mathematical operations.
        int sum = number1 + number2;
        System.out.println("The sum of the number are: " + sum);

        /*
            Mini- Activity:

            1.Add a system out print line to ask the user for a number.
                create a new integer variable and store the value returned by using the scanner and its
                nextInt() method.
            2.Add a system out print line to ask the user for a number.
                create a new integer variable and store the value returned by using the scanner and its
                nextInt() method.
            3.Create a new integer variable to store the difference of the numbers that was input.
            4.Print the value of the difference between the input numbers

        */

        System.out.println("Enter a number:");
        int entry1 = scannerName.nextInt();
        System.out.println("Enter a number:");
        int entry2 = scannerName.nextInt();

        int diff = entry1 - entry2;
        System.out.println("The difference of the number are: " + diff);

    }
}
